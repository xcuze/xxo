//
//  PreparationViewController.m
//  xxo
//
//  Created by Florian Krüger on 13/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "PreparationViewController.h"
#import "Game.h"
#import "GameViewController.h"

@interface PreparationViewController ()

@property (nonatomic, strong, readwrite) Game *game;

@end

@implementation PreparationViewController

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [self.player1NameTextfield becomeFirstResponder];
}

#pragma mark - NAVIGATION

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([[segue destinationViewController] isKindOfClass:[GameViewController class]]) {
    GameViewController *gameViewController = (GameViewController *)[segue destinationViewController];
    gameViewController.game = self.game;
  }
}

#pragma mark - TARGET

- (IBAction)start:(UIButton *)sender
{
  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:self.player1NameTextfield.text
                                andPlayer2Name:self.player2NameTextfield.text
                                         error:&error];

  if (error) {
    [[[UIAlertView alloc] initWithTitle:@""
                                message:error.localizedDescription
                               delegate:nil
                      cancelButtonTitle:[error.localizedRecoveryOptions firstObject]
                      otherButtonTitles:nil] show];
  } else {
    self.game = game;
    [self performSegueWithIdentifier:@"startGame" sender:sender];
  }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  if (textField == self.player1NameTextfield) {
    [self.player2NameTextfield becomeFirstResponder];
  }
  else if (textField == self.player2NameTextfield) {
    [self.view endEditing:TRUE];
    [self start:nil];
  }

  return FALSE;
}

@end
