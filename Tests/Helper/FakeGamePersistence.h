//
//  FakeGamePersistence.h
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GamePersistence.h"

@interface FakeGamePersistence : NSObject <GamePersistence>

@property (nonnull, nonatomic, strong, readwrite) NSMutableDictionary *data;

@end
