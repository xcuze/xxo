//
//  Game_OpenRows.h
//  xxo
//
//  Created by Florian Krüger on 14/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "Game.h"

@interface Game ()

@property (nonatomic, strong, readwrite) NSArray *rows;

@end
