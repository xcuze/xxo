//
//  GameTests.m
//  xxo
//
//  Created by Florian Krüger on 14/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "Game.h"

@interface GameCreationTests : XCTestCase

@end

@implementation GameCreationTests

- (void)testSuccessfulGameCreation
{
  NSString *player1Name = @"lorem";
  NSString *player2Name = @"ipsum";

  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:player1Name andPlayer2Name:player2Name error:&error];

  XCTAssertNotNil(game);
  XCTAssertNil(error);

  XCTAssertEqualObjects(game.player1Name, player1Name);
  XCTAssertEqualObjects(game.player2Name, player2Name);
  XCTAssertEqual(game.currentPlayer, GamePlayerOne);
}

- (void)testErrorParamBeingSetToNilOnSuccessfulGameCreation
{
  NSError *error = [NSError errorWithDomain:@"org.example.errors" code:1 userInfo:nil];

  Game *game = [Game createGameWithPlayer1Name:@"lorem" andPlayer2Name:@"ipsum" error:&error];

  XCTAssertNotNil(game);
  XCTAssertNil(error);
}

- (void)testMissingPlayerNames
{
  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:nil andPlayer2Name:nil error:&error];

  XCTAssertNil(game);
  XCTAssertNotNil(error);
  XCTAssertEqual(error.domain, XXOGameErrorDomain);
  XCTAssertEqual(error.code, XXOGameErrorPlayerNamesAreMissing);
  XCTAssertEqualObjects(error.localizedDescription, @"please enter a name for the players");
}

- (void)testMissingPlayer1Name
{
  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:nil andPlayer2Name:@"lorem" error:&error];

  XCTAssertNil(game);
  XCTAssertNotNil(error);
  XCTAssertEqual(error.domain, XXOGameErrorDomain);
  XCTAssertEqual(error.code, XXOGameErrorPlayer1NameIsMissing);
  XCTAssertEqualObjects(error.localizedDescription, @"please enter a name for player 1");
}

- (void)testMissingPlayer2Name
{
  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:@"lorem" andPlayer2Name:nil error:&error];

  XCTAssertNil(game);
  XCTAssertNotNil(error);
  XCTAssertEqual(error.domain, XXOGameErrorDomain);
  XCTAssertEqual(error.code, XXOGameErrorPlayer2NameIsMissing);
  XCTAssertEqualObjects(error.localizedDescription, @"please enter a name for player 2");
}

- (void)testInitialBoardState
{
  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:@"lorem" andPlayer2Name:@"ipsum" error:&error];

  for (NSUInteger row = 0; row <= 2; row++) {
    for (NSUInteger column = 0; column <= 2; column++) {
      GamePlayer player = [game playerAtRow:row andColumn:column];
      XCTAssertEqual(player, GamePlayerNone);
    }
  }
}

- (void)testInitialGameState
{
  NSError *error;
  Game *game = [Game createGameWithPlayer1Name:@"lorem" andPlayer2Name:@"ipsum" error:&error];

  XCTAssertEqual(game.state, GameStateStarted);
}

@end
