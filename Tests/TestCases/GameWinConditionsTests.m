//
//  GameWinConditionsTests.m
//  xxo
//
//  Created by Florian Krüger on 14/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "Game.h"
#import "Game_OpenRows.h"

@interface GameWinConditionsTests : XCTestCase

@property (nonatomic, strong, readwrite) Game *game;

@end

@implementation GameWinConditionsTests

- (void)setUp
{
  [super setUp];
  self.game = [Game createGameWithPlayer1Name:@"lorem" andPlayer2Name:@"ipsum" error:nil];
}

- (void)tearDown
{
  self.game = nil;
  [super tearDown];
}

- (void)testWinRow0
{
  self.game.rows = @[
                     @[@(GamePlayerOne),  @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy
                     ];
  [self.game currentPlayerDidSelectRow:0 andColumn:2 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinRow1
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerOne),  @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy
                     ];
  [self.game currentPlayerDidSelectRow:1 andColumn:2 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinRow2
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerOne),  @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:2 andColumn:2 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinColumn0
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerOne),  @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerOne),  @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:0 andColumn:0 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinColumn1
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:0 andColumn:1 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinColumn2
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerOne) ].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerOne) ].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:0 andColumn:2 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinDiag0
{
  self.game.rows = @[
                     @[@(GamePlayerOne),  @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:2 andColumn:2 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testWinDiag1
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerNone), @(GamePlayerOne),  @(GamePlayerNone)].mutableCopy,
                     @[@(GamePlayerOne),  @(GamePlayerNone), @(GamePlayerNone)].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:0 andColumn:2 error:nil];

  XCTAssertEqual(self.game.state, GameStatePlayerOneWon);
}

- (void)testDrawn
{
  self.game.rows = @[
                     @[@(GamePlayerNone), @(GamePlayerTwo), @(GamePlayerOne)].mutableCopy,
                     @[@(GamePlayerOne),  @(GamePlayerTwo), @(GamePlayerOne)].mutableCopy,
                     @[@(GamePlayerTwo),  @(GamePlayerOne), @(GamePlayerTwo)].mutableCopy,
                     ];
  [self.game currentPlayerDidSelectRow:0 andColumn:0 error:nil];

  XCTAssertEqual(self.game.state, GameStateDrawn);
}

@end
