//
//  DefaultGamePersistence.m
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "DefaultGamePersistence.h"
#import "DefaultGamePersistenceItem.h"

NSString * const kXXOPlayerNames = @"org.projectserver.xxo.store.playernames";
NSString * const kXXOWinsKey = @"org.projectserver.xxo.store.wins";
NSString * const kXXODrawsKey = @"org.projectserver.xxo.store.draws";
NSString * const kXXOLossesKey = @"org.projectserver.xxo.store.losses";

@implementation DefaultGamePersistence

#pragma mark - GamePersistence

- (nonnull NSArray *)allPlayerNames
{
  NSArray *playerNames = [[NSUserDefaults standardUserDefaults] arrayForKey:kXXOPlayerNames];
  if (nil == playerNames) {
    playerNames = @[];
  }
  return playerNames;
}

- (nonnull id <GamePersistenceItem>)itemForPlayerName:(nonnull NSString *)playerName
{
  NSDictionary *rawItem = [[NSUserDefaults standardUserDefaults] dictionaryForKey:playerName];

  if (nil == rawItem) {
    return [DefaultGamePersistenceItem itemWithPlayerName:playerName wins:0 draws:0 andLosses:0];
  } else {
    NSNumber *rawWins = rawItem[kXXOWinsKey];
    NSNumber *rawDraws = rawItem[kXXODrawsKey];
    NSNumber *rawLosses = rawItem[kXXOLossesKey];

    NSUInteger wins   = (nil == rawWins)    ? 0 : [rawWins unsignedIntegerValue];
    NSUInteger draws  = (nil == rawDraws)   ? 0 : [rawDraws unsignedIntegerValue];
    NSUInteger losses = (nil == rawLosses)  ? 0 : [rawLosses unsignedIntegerValue];

    return [DefaultGamePersistenceItem itemWithPlayerName:playerName wins:wins draws:draws andLosses:losses];
  }
}

- (void)storeItem:(nonnull id <GamePersistenceItem>)item
{
  NSDictionary *rawItem = @{kXXOWinsKey:    @([item wins]),
                            kXXODrawsKey:   @([item draws]),
                            kXXOLossesKey:  @([item losses])};

  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setObject:rawItem forKey:[item playerName]];

  NSArray *playerNames = [userDefaults arrayForKey:kXXOPlayerNames];
  if (nil == playerNames) {
    playerNames = @[];
  }

  if (![playerNames containsObject:[item playerName]]) {
    NSMutableArray *mutablePlayerNames = [playerNames mutableCopy];
    [mutablePlayerNames addObject:[item playerName]];
    [userDefaults setObject:mutablePlayerNames forKey:kXXOPlayerNames];
  }

  [userDefaults synchronize];
}

@end
