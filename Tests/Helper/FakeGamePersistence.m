//
//  FakeGamePersistence.m
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "FakeGamePersistence.h"
#import "FakeGamePersistenceItem.h"

@implementation FakeGamePersistence

- (instancetype)init
{
  self = [super init];
  if (self) {
    _data = [NSMutableDictionary dictionary];
  }
  return self;
}

#pragma mark - GamePersistence

- (nonnull NSArray *)allPlayerNames
{
  return [self.data allKeys];
}

- (id <GamePersistenceItem>)itemForPlayerName:(NSString *)playerName
{
  FakeGamePersistenceItem *item = self.data[playerName];
  if (nil == item) {
    item = [[FakeGamePersistenceItem alloc] init];
    item.playerName = playerName;
    item.wins = 0;
    item.draws = 0;
    item.losses = 0;
    return item;
  }
  return item;
}

- (void)storeItem:(id <GamePersistenceItem>)item
{
  self.data[[item playerName]] = item;
}

@end
