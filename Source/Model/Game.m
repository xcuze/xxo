//
//  Game.m
//  xxo
//
//  Created by Florian Krüger on 14/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "Game.h"

NSString * const XXOGameErrorDomain = @"org.projectserver.xxo.errors.game";
NSUInteger const XXOGameErrorNoError __attribute__((used)) = 0;
NSUInteger const XXOGameErrorPlayerNamesAreMissing __attribute__((used)) = 1;
NSUInteger const XXOGameErrorPlayer1NameIsMissing __attribute__((used)) = 2;
NSUInteger const XXOGameErrorPlayer2NameIsMissing __attribute__((used)) = 3;
NSUInteger const XXOGameErrorFieldAlreadyTaken __attribute__((used)) = 4;
NSUInteger const XXOGameErrorRowIndexInvalid __attribute__((used)) = 5;
NSUInteger const XXOGameErrorColumnIndexInvalid __attribute__((used)) = 6;

@interface Game ()

@property (nonatomic, assign, readwrite) GameState state;
@property (nonatomic, strong, readwrite) NSString *player1Name;
@property (nonatomic, strong, readwrite) NSString *player2Name;
@property (nonatomic, assign, readwrite) GamePlayer currentPlayer;

@property (nonatomic, strong, readwrite) NSArray *rows;

@end

@implementation Game

#pragma mark - FACTORY

+ (instancetype)createGameWithPlayer1Name:(NSString *)player1Name andPlayer2Name:(NSString *)player2Name error:(NSError **)error
{
  Game *game = nil;

  BOOL player1HasAName = player1Name.length != 0;
  BOOL player2HasAName = player2Name.length != 0;

  NSString *message = nil;
  NSUInteger code = XXOGameErrorNoError;

  if (!player1HasAName && !player2HasAName) {
    message = @"please enter a name for the players";
    code = XXOGameErrorPlayerNamesAreMissing;
  }
  else if (!player1HasAName) {
    message = @"please enter a name for player 1";
    code = XXOGameErrorPlayer1NameIsMissing;
  } else if (!player2HasAName) {
    message = @"please enter a name for player 2";
    code = XXOGameErrorPlayer2NameIsMissing;
  }

  if (code == XXOGameErrorNoError) {
    game = [[Game alloc] init];
    game.player1Name = player1Name;
    game.player2Name = player2Name;
    game.currentPlayer = GamePlayerOne;

    if (error) {
      *error = nil;
    }
  } else {
    if (error) {
      NSDictionary *userInfo = @{NSLocalizedDescriptionKey: message,
                                 NSLocalizedRecoveryOptionsErrorKey: @[@"OK"]};
      *error = [NSError errorWithDomain:XXOGameErrorDomain
                                   code:code
                               userInfo:userInfo];
    }
  }

  return game;
}

#pragma mark - INITIALIZATION

- (instancetype)init
{
  self = [super init];
  if (self) {
    _state = GameStateStarted;

    NSMutableArray *row0 = [NSMutableArray arrayWithCapacity:3];
    NSMutableArray *row1 = [NSMutableArray arrayWithCapacity:3];
    NSMutableArray *row2 = [NSMutableArray arrayWithCapacity:3];
    _rows = @[row0, row1, row2];

    for (NSUInteger rowIndex = 0; rowIndex <= 2; rowIndex++) {
      for (NSUInteger columnIndex = 0; columnIndex <= 2; columnIndex++) {
        NSMutableArray *row = _rows[rowIndex];
        [row addObject:@(GamePlayerNone)];
      }
    }
  }
  return self;
}

#pragma mark - GAMEPLAY

- (BOOL)currentPlayerDidSelectRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex error:(NSError **)error
{
  NSString *message = nil;
  NSUInteger code = XXOGameErrorNoError;
  GamePlayer owner = [self playerAtRow:rowIndex andColumn:columnIndex];

  // check already taken
  if (owner != GamePlayerNone) {
    message = @"the specified field is already taken";
    code = XXOGameErrorFieldAlreadyTaken;
  }

  // check valid row
  if (code == XXOGameErrorNoError && rowIndex > 2) {
    message = @"the specified row index is out of bounds (0..2)";
    code = XXOGameErrorRowIndexInvalid;
  }

  // check valid column
  if (code == XXOGameErrorNoError && columnIndex > 2) {
    message = @"the specified column index is out of bounds (0..2)";
    code = XXOGameErrorColumnIndexInvalid;
  }

  // select field for current user and switch user
  if (code == XXOGameErrorNoError) {
    NSMutableArray *row = self.rows[rowIndex];
    row[columnIndex] = @(self.currentPlayer);

    switch (self.currentPlayer) {
      case GamePlayerOne: {
        self.currentPlayer = GamePlayerTwo;
        break;
      }
      case GamePlayerTwo: {
        self.currentPlayer = GamePlayerOne;
        break;
      }
      case GamePlayerNone: {
        break;
      }
    }
  }

  // return
  if (code == XXOGameErrorNoError) {
    if (error) {
      *error = nil;
    }
    [self refreshGameState];
    return TRUE;
  } else {
    if (error) {
      NSDictionary *userInfo = @{NSLocalizedDescriptionKey:message,
                                 NSLocalizedRecoveryOptionsErrorKey: @[@"OK"]};
      *error = [NSError errorWithDomain:XXOGameErrorDomain
                                   code:code
                               userInfo:userInfo];
    }
    return FALSE;
  }
}

#pragma mark - GAMESTATE

- (void)refreshGameState
{
  // check rows
  for (NSUInteger rowIndex = 0; rowIndex <= 2; rowIndex++) {
    NSArray *row = self.rows[rowIndex];
    if ([self checkLine:row]) { return; }
  }

  // check columns
  for (NSUInteger columnIndex = 0; columnIndex <= 2; columnIndex++) {
    NSArray *column = @[self.rows[0][columnIndex], self.rows[1][columnIndex], self.rows[2][columnIndex]];
    if ([self checkLine:column]) { return; }
  }

  NSArray *diag;

  // check diag 0/0 -> 2/2
  diag = @[self.rows[0][0], self.rows[1][1], self.rows[2][2]];
  if ([self checkLine:diag]) { return; }

  // check diag 2/0 -> 0/2
  diag = @[self.rows[2][0], self.rows[1][1], self.rows[0][2]];
  if ([self checkLine:diag]) { return; }

  // check draw
  NSUInteger untakenFieldsCount = 0;
  for (NSUInteger rowIndex = 0; rowIndex <= 2; rowIndex++) {
    for (NSUInteger columnIndex = 0; columnIndex <= 2; columnIndex++) {
      if ([self.rows[rowIndex][columnIndex] unsignedIntegerValue] == GamePlayerNone) {
        untakenFieldsCount++;
      }
    }
  }

  if (untakenFieldsCount == 0) {
    self.state = GameStateDrawn;
  }
}

- (BOOL)checkLine:(NSArray *)line
{
  NSUInteger value0 = [line[0] unsignedIntegerValue];
  NSUInteger value1 = [line[1] unsignedIntegerValue];
  NSUInteger value2 = [line[2] unsignedIntegerValue];

  BOOL lineIsIdentical = (value0 == value1) && (value1 == value2);
  BOOL atLeastOneFieldIsTaken = value0 != GamePlayerNone;
  BOOL lineHasWinner = lineIsIdentical && atLeastOneFieldIsTaken;

  if (lineHasWinner) { [self winnerFromFieldValue:value0]; }
  return lineHasWinner;
}

- (void)winnerFromFieldValue:(NSUInteger)fieldValue
{
  switch (fieldValue) {
    case GamePlayerOne: {
      self.state = GameStatePlayerOneWon;
      break;
    }
    case GamePlayerTwo: {
      self.state = GameStatePlayerTwoWon;
      break;
    }
  }
}

#pragma mark - BOARD

- (GamePlayer)playerAtRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex
{
  if (rowIndex <= 2 && columnIndex <= 2) {
    NSArray *row = self.rows[rowIndex];
    NSNumber *field = row[columnIndex];
    NSUInteger value = [field unsignedIntegerValue];

    switch (value) {
      case GamePlayerOne: {
        return GamePlayerOne;
      }
      case GamePlayerTwo: {
        return GamePlayerTwo;
      }
      default:
        return GamePlayerNone;
    }
  }
  return GamePlayerNone;
}

@end
