//
//  Game.h
//  xxo
//
//  Created by Florian Krüger on 14/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString * const XXOGameErrorDomain;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorNoError;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorPlayerNamesAreMissing;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorPlayer1NameIsMissing;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorPlayer2NameIsMissing;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorFieldAlreadyTaken;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorRowIndexInvalid;
FOUNDATION_EXTERN NSUInteger const XXOGameErrorColumnIndexInvalid;

typedef NS_ENUM(NSUInteger, GamePlayer) {
  GamePlayerNone  = 0,
  GamePlayerOne   = 1,
  GamePlayerTwo   = 2
};

typedef NS_ENUM(NSUInteger, GameState) {
  GameStateStarted,
  GameStateDrawn,
  GameStatePlayerOneWon,
  GameStatePlayerTwoWon
};

@interface Game : NSObject

@property (nonatomic, assign, readonly) GameState state;
@property (nonatomic, strong, readonly) NSString *player1Name;
@property (nonatomic, strong, readonly) NSString *player2Name;
@property (nonatomic, assign, readonly) GamePlayer currentPlayer;

/** @name Factory */

/** Creates a new @c Game object.
 *
 *  @param player1Name The name of the first player, must not be @c nil.
 *  @param player2Name The name of the second player, must not be @c nil.
 *  @param error Optional output parameter which is set to @c nil if the game could be created, or 
 *               it contains error information why the game couldn't be created.
 */
+ (instancetype)createGameWithPlayer1Name:(NSString *)player1Name andPlayer2Name:(NSString *)player2Name error:(NSError **)error;

/** @name GamePlay */

/** Call this when the @c currentPlayer did select a field. It will check the validity of the move
 *  and update the game board accordingly. If everything was correct the game state ( @c winner ) is
 *  updated as well.
 *
 *  @param rowIndex The row of the selected field. Must be in range 0 to 2.
 *  @param columnIndex The column of the selected field. Must be in range 0 to 2.
 *  @param error Optional output parameter which is set to @c nil if the move was valid, or it 
 *               contains error information why the move was invalid.
 *  @return @c TRUE if the move was valid and the game board as well as the game state have been
 *          updated. @c FALSE if the move was invalid. Check @c error for details.
 */
- (BOOL)currentPlayerDidSelectRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex error:(NSError **)error;

/** @name Board */

/** This method returns the player to whom the field belongs for all rows from 0 to 2 and all 
 *  columns from 0 to 2. All other row and columns return @c GamePlayerNone for the sake of 
 *  simplicity.
 *
 *  @param rowIndex The row under consideration. The valid range is 0 to 2.
 *  @param columnIndex The column under consideration. The valid range is 0 to 2.
 *  @return @c GamePlayerNone for all fields which have not been taken yet (or all invalid 
 *          row/column combinations), @c GamePlayerOne if the first player took the field or 
 *          @c GamePlayerTwo if the second player took the field.
 */
- (GamePlayer)playerAtRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex;

@end
