//
//  GamePlayTests.m
//  xxo
//
//  Created by Florian Krüger on 14/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "Game.h"

@interface GamePlayTests : XCTestCase

@property (nonatomic, strong, readwrite) Game *game;

@end

@implementation GamePlayTests

- (void)setUp
{
  [super setUp];
  self.game = [Game createGameWithPlayer1Name:@"player1" andPlayer2Name:@"player2" error:nil];
}

- (void)tearDown
{
  self.game = nil;
  [super tearDown];
}

- (void)testMakeValidMove_0_0
{
  [self successfullySelectRow:0 andColumn:0];
}

- (void)testMakeValidMove_1_0
{
  [self successfullySelectRow:1 andColumn:0];
}

- (void)testMakeValidMove_2_0
{
  [self successfullySelectRow:2 andColumn:0];
}

- (void)testMakeValidMove_0_1
{
  [self successfullySelectRow:0 andColumn:1];
}

- (void)testMakeValidMove_1_1
{
  [self successfullySelectRow:1 andColumn:1];
}

- (void)testMakeValidMove_2_1
{
  [self successfullySelectRow:2 andColumn:1];
}

- (void)testMakeValidMove_0_2
{
  [self successfullySelectRow:0 andColumn:2];
}

- (void)testMakeValidMove_1_2
{
  [self successfullySelectRow:1 andColumn:2];
}

- (void)testMakeValidMove_2_2
{
  [self successfullySelectRow:2 andColumn:2];
}

- (void)testMakeInvalidMoveRowOutOfRange
{
  NSError *error = [self unsuccessfullySelectRow:3 andColumn:0];
  XCTAssertNotNil(error);
  XCTAssertEqual(error.code, XXOGameErrorRowIndexInvalid);
}

- (void)testMakeInvalidMoveColumnOutOfRange
{
  NSError *error = [self unsuccessfullySelectRow:0 andColumn:3];
  XCTAssertNotNil(error);
  XCTAssertEqual(error.code, XXOGameErrorColumnIndexInvalid);
}

- (void)testMakeInvalidMoveFieldAlreadyTaken
{
  // pre-select 1/1
  [self successfullySelectRow:1 andColumn:1];

  // try to select 1/1 again
  NSError *error = [self unsuccessfullySelectRow:1 andColumn:1];
  XCTAssertNotNil(error);
  XCTAssertEqual(error.code, XXOGameErrorFieldAlreadyTaken);
}

- (void)successfullySelectRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex
{
  NSError *error;
  GamePlayer before = [self.game currentPlayer];
  BOOL success = [self.game currentPlayerDidSelectRow:rowIndex andColumn:columnIndex error:&error];
  GamePlayer owner = [self.game playerAtRow:rowIndex andColumn:columnIndex];
  GamePlayer after = [self.game currentPlayer];

  XCTAssertEqual(success, TRUE);
  XCTAssertNil(error);
  XCTAssertNotEqual(after, GamePlayerNone);
  XCTAssertEqual(owner, before);
  XCTAssertNotEqual(after, before);
  XCTAssertEqual(self.game.state, GameStateStarted);
}

- (NSError *)unsuccessfullySelectRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex
{
  NSError *error;
  GamePlayer before = [self.game currentPlayer];
  GamePlayer ownerBefore = [self.game playerAtRow:rowIndex andColumn:columnIndex];
  BOOL success = [self.game currentPlayerDidSelectRow:rowIndex andColumn:columnIndex error:&error];
  GamePlayer ownerAfter = [self.game playerAtRow:rowIndex andColumn:columnIndex];
  GamePlayer after = [self.game currentPlayer];

  XCTAssertEqual(success, FALSE);
  XCTAssertEqual(ownerAfter, ownerBefore);
  XCTAssertEqual(after, before);

  return error;
}

- (void)testErrorParamBeingSetToNilOnValidMove
{
  NSError *error = [NSError errorWithDomain:@"org.example.errors" code:1 userInfo:nil];
  [self.game currentPlayerDidSelectRow:0 andColumn:0 error:&error];

  XCTAssertNil(error);
}

- (void)testInvalidRowReturnsNone
{
  GamePlayer player = [self.game playerAtRow:3 andColumn:0];
  XCTAssertEqual(player, GamePlayerNone);
}

- (void)testInvalidColumnReturnsNone
{
  GamePlayer player = [self.game playerAtRow:0 andColumn:3];
  XCTAssertEqual(player, GamePlayerNone);
}

- (void)testInvalidRowAndColumnReturnsNone
{
  GamePlayer player = [self.game playerAtRow:3 andColumn:3];
  XCTAssertEqual(player, GamePlayerNone);
}

@end
