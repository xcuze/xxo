//
//  GamePersistenceItem.h
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GamePersistenceItem <NSObject>

+ (nonnull id <GamePersistenceItem>)itemWithPlayerName:(nonnull NSString *)playerName wins:(NSUInteger)wins draws:(NSUInteger)draws andLosses:(NSUInteger)losses;

- (nonnull NSString *)playerName;

- (NSUInteger)wins;
- (NSUInteger)draws;
- (NSUInteger)losses;

- (void)addWin;
- (void)addDraw;
- (void)addLoss;

@end
