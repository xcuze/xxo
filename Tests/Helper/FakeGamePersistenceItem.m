//
//  FakeGamePersistenceItem.m
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "FakeGamePersistenceItem.h"

@implementation FakeGamePersistenceItem

+ (nonnull id <GamePersistenceItem>)itemWithPlayerName:(nonnull NSString *)playerName wins:(NSUInteger)wins draws:(NSUInteger)draws andLosses:(NSUInteger)losses
{
  FakeGamePersistenceItem *item = [[FakeGamePersistenceItem alloc] init];
  item.playerName = playerName;
  item.wins = wins;
  item.draws = draws;
  item.losses = losses;
  return item;
}

- (void)addWin
{
  _wins = _wins + 1;
}

- (void)addDraw
{
  _draws = _draws + 1;
}

- (void)addLoss
{
  _losses = _losses + 1;
}

@end
