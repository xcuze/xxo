//
//  HighscoreStore.m
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "HighscoreStore.h"
#import "DefaultGamePersistence.h"

@interface HighscoreStore ()

@property (nonnull, nonatomic, strong, readwrite) id <GamePersistence> gamePersistence;

@end

@implementation HighscoreStore

+ (nullable instancetype)defaultHighscoreStore
{
  id <GamePersistence> gamePersistence = [[DefaultGamePersistence alloc] init];
  return [[self alloc] initWithGamePersistence:gamePersistence];
}

- (nullable instancetype)initWithGamePersistence:(nonnull id <GamePersistence>)gamePersistence
{
  self = [super init];
  if (self) {
    _gamePersistence = gamePersistence;
  }
  return self;
}

- (void)storeResult:(GameResult)gameResult forPlayer1:(nonnull NSString *)playerOneName andPlayerTwo:(nonnull NSString *)playerTwoName
{
  id <GamePersistenceItem> playerOneItem = [self.gamePersistence itemForPlayerName:playerOneName];
  id <GamePersistenceItem> playerTwoItem = [self.gamePersistence itemForPlayerName:playerTwoName];

  switch (gameResult) {
    case GameResultDrawn: {
      [playerOneItem addDraw];
      [playerTwoItem addDraw];
      break;
    }
    case GameResultPlayerOneWon: {
      [playerOneItem addWin];
      [playerTwoItem addLoss];
      break;
    }
    case GameResultPlayerTwoWon: {
      [playerOneItem addLoss];
      [playerTwoItem addWin];
      break;
    }
  }

  [self.gamePersistence storeItem:playerOneItem];
  [self.gamePersistence storeItem:playerTwoItem];
}

- (nonnull id <GamePersistenceItem>)highscoreForPlayerNamed:(nonnull NSString *)playerName
{
  return [self.gamePersistence itemForPlayerName:playerName];
}

- (nonnull NSArray *)allPlayerNames
{
  return [self.gamePersistence allPlayerNames];
}

- (nonnull NSArray *)allHighscores
{
  return [self orderHighscores:[self allHighscoresUnordered]];
}

#pragma mark - Helper

- (NSArray *)allHighscoresUnordered
{
  NSArray *allPlayerNames = [self allPlayerNames];
  NSMutableArray *allResults = [NSMutableArray arrayWithCapacity:[allPlayerNames count]];

  for (NSString *playerName in allPlayerNames) {
    [allResults addObject:[self highscoreForPlayerNamed:playerName]];
  }

  return [NSArray arrayWithArray:allResults];
}

- (NSArray *)orderHighscores:(NSArray *)highscores
{
  return [highscores sortedArrayUsingComparator:^NSComparisonResult(id <GamePersistenceItem> a, id <GamePersistenceItem> b) {
    NSInteger scoreA = [a wins] - [a losses];
    NSInteger scoreB = [b wins] - [b losses];

    if (scoreA > scoreB) {
      return NSOrderedAscending;
    }
    else if (scoreA < scoreB) {
      return NSOrderedDescending;
    }
    else {
      return [[a playerName] localizedCompare:[b playerName]];
    }
  }];
}

@end
