//
//  GameViewController.h
//  xxo
//
//  Created by Florian Krüger on 13/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"

@interface GameViewController : UIViewController

@property (nonatomic, strong, readwrite) Game *game;

@property (weak, nonatomic) IBOutlet UILabel *playerOneNameTop;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoNameTop;
@property (weak, nonatomic) IBOutlet UILabel *currentPlayerNameTop;

@property (weak, nonatomic) IBOutlet UILabel *playerOneNameLeft;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoNameLeft;
@property (weak, nonatomic) IBOutlet UILabel *currentPlayerNameLeft;

@property (weak, nonatomic) IBOutlet UIButton *button_0_0;
@property (weak, nonatomic) IBOutlet UIButton *button_0_1;
@property (weak, nonatomic) IBOutlet UIButton *button_0_2;
@property (weak, nonatomic) IBOutlet UIButton *button_1_0;
@property (weak, nonatomic) IBOutlet UIButton *button_1_1;
@property (weak, nonatomic) IBOutlet UIButton *button_1_2;
@property (weak, nonatomic) IBOutlet UIButton *button_2_0;
@property (weak, nonatomic) IBOutlet UIButton *button_2_1;
@property (weak, nonatomic) IBOutlet UIButton *button_2_2;

- (IBAction)touched_0_0:(UIButton *)sender;
- (IBAction)touched_0_1:(UIButton *)sender;
- (IBAction)touched_0_2:(UIButton *)sender;
- (IBAction)touched_1_0:(UIButton *)sender;
- (IBAction)touched_1_1:(UIButton *)sender;
- (IBAction)touched_1_2:(UIButton *)sender;
- (IBAction)touched_2_0:(UIButton *)sender;
- (IBAction)touched_2_1:(UIButton *)sender;
- (IBAction)touched_2_2:(UIButton *)sender;

@end
