//
//  HighscoreStore.h
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GamePersistence.h"

typedef NS_ENUM(NSUInteger, GameResult) {
  GameResultDrawn         = 0,
  GameResultPlayerOneWon  = 1,
  GameResultPlayerTwoWon  = 2,
};

@interface HighscoreStore : NSObject

@property (nonnull, nonatomic, strong, readonly) id <GamePersistence> gamePersistence;

/** Creates a @c HighscoreStore instance which uses the default GamePersistence implementation.
 *
 *  @return an instance of @c HighscoreStore
 */
+ (nullable instancetype)defaultHighscoreStore;

/** Creates a @c HighscoreStore which uses the provided @c GamePersistence instance to retrieve
 *  and store highscores.
 *
 *  @param gamePersistence an instance of a @c GamePersistence implementation
 *  @return an instance of @c HighscoreStore
 */
- (nullable instancetype)initWithGamePersistence:(nonnull id <GamePersistence>)gamePersistence;

/** Writes a game result to disk.
 *
 *  @param gameResult The result of the game to be persisted.
 *  @param playerOneName The name of the first player.
 *  @param playerTwoName The name of the second player.
 *  @param error Optional output parameter which is set to @c nil if the result could be persisted, 
 *               or it contains error information why the result couldn't be persisted.
 */
- (void)storeResult:(GameResult)gameResult forPlayer1:(nonnull NSString *)playerOneName andPlayerTwo:(nonnull NSString *)playerTwoName;

/** Retrieves the highscore for a specific player from the store. The method always returns a valid
 *  @c GamePersistenceItem, regardless of whether there are highscores stored for the player.
 *
 *  @param playerName the name of the player whose highscore should be fetched
 *  @return a @c GamePersistenceItem that contains the highscore for the given player name
 */
- (nonnull id <GamePersistenceItem>)highscoreForPlayerNamed:(nonnull NSString *)playerName;

/** Provides a list of all Player for which highscores are stored. The list isn't ordered in any
 *  particular way. If there aren't any scores stored yet, the list is empty.
 *
 *  @return a @c NSArray containing @c NSString player names
 */
- (nonnull NSArray *)allPlayerNames;

/** Retrieves all results currently stored in the persistence, ordered by the 'success' of the 
 *  players. 
 *
 *  The success is of a player is calculated as follows:
 *  - a win adds 1 to the success score
 *  - a draw is ignored
 *  - a loss subtracts one from the score
 *
 *  Players with equal success are ordered alphabetically.
 *
 *  @return a list of @c GamePersistenceItem instances
 */
- (nonnull NSArray *)allHighscores;

@end
