//
//  DefaultGamePersistenceItem.m
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "DefaultGamePersistenceItem.h"

@interface DefaultGamePersistenceItem ()

@property (nonnull, nonatomic, strong, readwrite) NSString *playerName;
@property (nonatomic, assign, readwrite) NSUInteger wins;
@property (nonatomic, assign, readwrite) NSUInteger draws;
@property (nonatomic, assign, readwrite) NSUInteger losses;

@end

@implementation DefaultGamePersistenceItem

- (instancetype)initWithPlayerName:(nonnull NSString *)playerName wins:(NSUInteger)wins draws:(NSUInteger)draws andLosses:(NSUInteger)losses
{
  self = [super init];
  if (self) {
    _playerName = playerName;
    _wins = wins;
    _draws = draws;
    _losses = losses;
  }
  return self;
}

#pragma mark - GamePersistenceItem

+ (nonnull id <GamePersistenceItem>)itemWithPlayerName:(nonnull NSString *)playerName wins:(NSUInteger)wins draws:(NSUInteger)draws andLosses:(NSUInteger)losses
{
  return [[DefaultGamePersistenceItem alloc] initWithPlayerName:playerName wins:wins draws:draws andLosses:losses];
}

- (nonnull NSString *)playerName
{
  return _playerName;
}

- (NSUInteger)wins
{
  return _wins;
}

- (NSUInteger)draws
{
  return _draws;
}

- (NSUInteger)losses
{
  return _losses;
}

- (void)addWin
{
  _wins = _wins + 1;
}

- (void)addDraw
{
  _draws = _draws + 1;
}

- (void)addLoss
{
  _losses = _losses + 1;
}

@end
