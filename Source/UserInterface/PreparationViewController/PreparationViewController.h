//
//  PreparationViewController.h
//  xxo
//
//  Created by Florian Krüger on 13/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreparationViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *player1NameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *player2NameTextfield;

- (IBAction)start:(UIButton *)sender;

@end
