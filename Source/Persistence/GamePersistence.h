//
//  GamePersistence.h
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GamePersistenceItem.h"

@protocol GamePersistence <NSObject>

/** Provides a list of all Player for which highscores are stored. The list isn't ordered in any
 *  particular way. If there aren't any scores stored yet, the list is empty.
 *
 *  @return a @c NSArray containing @c NSString player names
 */
- (nonnull NSArray *)allPlayerNames;

/** Retrieves the highscore for a player identified by a given name. This method always returns a 
 *  valid @c GamePersistenceItem instance even if there aren't any highscores stored for a player
 *  with the given name (in this case wins, draws and losses are 0).
 *
 *  @param playerName the name of the player for which the highscore should be retrieved
 *  @returm a @c GamePersistenceItem implementation instance
 */
- (nonnull id <GamePersistenceItem>)itemForPlayerName:(nonnull NSString *)playerName;

/** Stores a given @c GamePersistenceItem in the persistence. The item is immediately stored.
 *
 *  @param item a valid @c GamePersistenceItem implementation instance
 */
- (void)storeItem:(nonnull id <GamePersistenceItem>)item;

@end
