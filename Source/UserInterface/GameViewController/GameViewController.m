//
//  GameViewController.m
//  xxo
//
//  Created by Florian Krüger on 13/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "GameViewController.h"
#import "HighscoreStore.h"

@interface GameViewController () <UIAlertViewDelegate>

@property (nonatomic, strong, readwrite) NSArray *rows;

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.rows = @[@[self.button_0_0, self.button_0_1, self.button_0_2],
                  @[self.button_1_0, self.button_1_1, self.button_1_2],
                  @[self.button_2_0, self.button_2_1, self.button_2_2]];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self refreshDisplay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GAME

- (void)refreshDisplay
{
  self.playerOneNameTop.text = _game.player1Name;
  self.playerOneNameLeft.text = _game.player1Name;

  self.playerTwoNameTop.text = _game.player2Name;
  self.playerTwoNameLeft.text = _game.player2Name;

  for (NSUInteger rowIndex = 0; rowIndex <= 2; rowIndex++) {
    for (NSUInteger columnIndex = 0; columnIndex <= 2; columnIndex++) {
      GamePlayer owner = [_game playerAtRow:rowIndex andColumn:columnIndex];
      UIButton *button = self.rows[rowIndex][columnIndex];

      switch (owner) {
        case GamePlayerNone: {
          button.backgroundColor = [UIColor whiteColor];
          break;
        }
        case GamePlayerOne: {
          button.backgroundColor = [UIColor blueColor];
          break;
        }
        case GamePlayerTwo: {
          button.backgroundColor = [UIColor redColor];
          break;
        }
      }
    }
  }

  switch (self.game.currentPlayer) {
    case GamePlayerNone:
      self.currentPlayerNameTop.text = nil;
      self.currentPlayerNameLeft.text = nil;
      break;
    case GamePlayerOne:
      self.currentPlayerNameTop.text = _game.player1Name;
      self.currentPlayerNameLeft.text = _game.player1Name;
      break;
    case GamePlayerTwo:
      self.currentPlayerNameTop.text = _game.player2Name;
      self.currentPlayerNameLeft.text = _game.player2Name;
      break;
  }

  NSString *title = nil;
  NSString *message = nil;

  switch (self.game.state) {
    case GameStateStarted: {
      // do nothing, just continue
      break;
    }
    case GameStateDrawn: {
      title = @"Drawn";
      message = @"No moves left. This game is drawn";
      break;
    }
    case GameStatePlayerOneWon: {
      title = @"Congratulations";
      message = [NSString stringWithFormat:@"%@ won", self.game.player1Name];
      break;
    }
    case GameStatePlayerTwoWon: {
      title = @"Congratulations";
      message = [NSString stringWithFormat:@"%@ won", self.game.player2Name];
      break;
    }
  }

  if (self.game.state != GameStateStarted) {
    [[[UIAlertView alloc] initWithTitle:title
                                message:message
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
    [self storeGameResult];
  }
}

- (void)storeGameResult
{
  HighscoreStore *store = [HighscoreStore defaultHighscoreStore];

  if (self.game.state == GameStateDrawn) {
    [store storeResult:GameResultDrawn forPlayer1:self.game.player1Name andPlayerTwo:self.game.player2Name];
  }
  else if (self.game.state == GameStatePlayerOneWon) {
    [store storeResult:GameResultPlayerOneWon forPlayer1:self.game.player1Name andPlayerTwo:self.game.player2Name];
  }
  else if (self.game.state == GameStatePlayerTwoWon) {
    [store storeResult:GameResultPlayerTwoWon forPlayer1:self.game.player1Name andPlayerTwo:self.game.player2Name];
  }
}

- (BOOL)selectFieldAtRow:(NSUInteger)rowIndex andColumn:(NSUInteger)columnIndex
{
  NSError *error;
  BOOL success = [self.game currentPlayerDidSelectRow:rowIndex andColumn:columnIndex error:&error];
  if (error) {
    [[[UIAlertView alloc] initWithTitle:nil
                                message:error.localizedDescription
                               delegate:nil
                      cancelButtonTitle:[error.localizedRecoveryOptions firstObject]
                      otherButtonTitles:nil] show];
  }
  [self refreshDisplay];
  return success;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
  [self performSegueWithIdentifier:@"finishGame" sender:self];
}

#pragma mark - TARGET

- (IBAction)touched_0_0:(UIButton *)sender
{
  if ([self selectFieldAtRow:0 andColumn:0]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_0_1:(UIButton *)sender
{
  if ([self selectFieldAtRow:0 andColumn:1]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_0_2:(UIButton *)sender
{
  if ([self selectFieldAtRow:0 andColumn:2]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_1_0:(UIButton *)sender
{
  if ([self selectFieldAtRow:1 andColumn:0]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_1_1:(UIButton *)sender
{
  if ([self selectFieldAtRow:1 andColumn:1]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_1_2:(UIButton *)sender
{
  if ([self selectFieldAtRow:1 andColumn:2]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_2_0:(UIButton *)sender
{
  if ([self selectFieldAtRow:2 andColumn:0]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_2_1:(UIButton *)sender
{
  if ([self selectFieldAtRow:2 andColumn:1]) {
    sender.userInteractionEnabled = FALSE;
  }
}

- (IBAction)touched_2_2:(UIButton *)sender
{
  if ([self selectFieldAtRow:2 andColumn:2]) {
    sender.userInteractionEnabled = FALSE;
  }
}

@end
