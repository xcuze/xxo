//
//  HighscoreViewController.m
//  xxo
//
//  Created by Florian Krüger on 16/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import "HighscoreViewController.h"
#import "HighscoreStore.h"
#import "GamePersistenceItem.h"

@interface HighscoreViewController ()

@property (nonatomic, strong, readwrite) NSArray *highscores;
@property (nonatomic, strong, readwrite) HighscoreStore *store;

@end

@implementation HighscoreViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super initWithCoder:coder];
  if (self) {
    _highscores = @[];
    _store = [HighscoreStore defaultHighscoreStore];
  }
  return self;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self refreshData];
}

#pragma mark - Persistence Access

- (void)refreshData
{
  _highscores = [_store allHighscores];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [self.highscores count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell"];

  id <GamePersistenceItem> item = self.highscores[indexPath.row];
  cell.textLabel.text = [NSString stringWithFormat:@"%li. %@", (indexPath.row + 1), [item playerName]];
  cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu / %lu / %lu",
                               (unsigned long)[item wins],
                               (unsigned long)[item draws],
                               (unsigned long)[item losses]];

  return cell;
}

@end
