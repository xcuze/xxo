//
//  HighscoreStoreTests.m
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "HighscoreStore.h"
#import "FakeGamePersistence.h"
#import "FakeGamePersistenceItem.h"

@interface HighscoreStoreTests : XCTestCase

@property (nonatomic, strong, readwrite) FakeGamePersistence *persistence;
@property (nonatomic, strong, readwrite) HighscoreStore *store;

@end

@implementation HighscoreStoreTests

- (void)setUp
{
  [super setUp];
  self.persistence = [[FakeGamePersistence alloc] init];
  self.store = [[HighscoreStore alloc] initWithGamePersistence:self.persistence];
}

- (void)tearDown
{
  self.persistence = nil;
  self.store = nil;
  [super tearDown];
}

- (void)testSavingGameResultPlayerOneWon
{
  NSString *playerOneName = @"lorem";
  NSString *playerTwoName = @"ipsum";

  [self.store storeResult:GameResultPlayerOneWon forPlayer1:playerOneName andPlayerTwo:playerTwoName];

  FakeGamePersistenceItem *playerOneItem = self.persistence.data[playerOneName];
  FakeGamePersistenceItem *playerTwoItem = self.persistence.data[playerTwoName];

  XCTAssertNotNil(playerOneItem);
  XCTAssertEqual(playerOneItem.wins, 1);
  XCTAssertEqual(playerOneItem.draws, 0);
  XCTAssertEqual(playerOneItem.losses, 0);

  XCTAssertNotNil(playerTwoItem);
  XCTAssertEqual(playerTwoItem.wins, 0);
  XCTAssertEqual(playerTwoItem.draws, 0);
  XCTAssertEqual(playerTwoItem.losses, 1);
}

- (void)testSavingGameResultDrawn
{
  NSString *playerOneName = @"lorem";
  NSString *playerTwoName = @"ipsum";

  [self.store storeResult:GameResultDrawn forPlayer1:playerOneName andPlayerTwo:playerTwoName];

  FakeGamePersistenceItem *playerOneItem = self.persistence.data[playerOneName];
  FakeGamePersistenceItem *playerTwoItem = self.persistence.data[playerTwoName];

  XCTAssertNotNil(playerOneItem);
  XCTAssertEqual(playerOneItem.wins, 0);
  XCTAssertEqual(playerOneItem.draws, 1);
  XCTAssertEqual(playerOneItem.losses, 0);

  XCTAssertNotNil(playerTwoItem);
  XCTAssertEqual(playerTwoItem.wins, 0);
  XCTAssertEqual(playerTwoItem.draws, 1);
  XCTAssertEqual(playerTwoItem.losses, 0);
}

- (void)testSavingGameResultPlayerTwoWon
{
  NSString *playerOneName = @"lorem";
  NSString *playerTwoName = @"ipsum";

  [self.store storeResult:GameResultPlayerTwoWon forPlayer1:playerOneName andPlayerTwo:playerTwoName];

  FakeGamePersistenceItem *playerOneItem = self.persistence.data[playerOneName];
  FakeGamePersistenceItem *playerTwoItem = self.persistence.data[playerTwoName];

  XCTAssertNotNil(playerOneItem);
  XCTAssertEqual(playerOneItem.wins, 0);
  XCTAssertEqual(playerOneItem.draws, 0);
  XCTAssertEqual(playerOneItem.losses, 1);

  XCTAssertNotNil(playerTwoItem);
  XCTAssertEqual(playerTwoItem.wins, 1);
  XCTAssertEqual(playerTwoItem.draws, 0);
  XCTAssertEqual(playerTwoItem.losses, 0);
}

- (void)testRetrievingResultsForSpecificPlayer
{
  NSString *playerName = @"lorem ipsum";
  NSUInteger wins = 1;
  NSUInteger draws = 2;
  NSUInteger losses = 3;

  FakeGamePersistenceItem *item = [[FakeGamePersistenceItem alloc] init];
  item.playerName = playerName;
  item.wins = wins;
  item.draws = draws;
  item.losses = losses;
  self.persistence.data[playerName] = item;

  id <GamePersistenceItem> persistenceItem = [self.store highscoreForPlayerNamed:playerName];

  XCTAssertNotNil(persistenceItem);
  XCTAssertEqualObjects([persistenceItem playerName], playerName);
  XCTAssertEqual([persistenceItem wins], wins);
  XCTAssertEqual([persistenceItem draws], draws);
  XCTAssertEqual([persistenceItem losses], losses);
}

- (void)testRetrieveAllPlayerNames
{
  NSArray *playerOneNames = @[@"a", @"b", @"c", @"d", @"e"];
  NSArray *playerTwoNames = @[@"f", @"g", @"h", @"i", @"j"];

  for (NSUInteger index = 0; index < [playerOneNames count]; index++) {
    [self.store storeResult:GameResultDrawn forPlayer1:playerOneNames[index] andPlayerTwo:playerTwoNames[index]];
  }

  NSArray *playerNames = [self.store allPlayerNames];

  XCTAssertNotNil(playerNames);
  XCTAssertEqual([playerNames count], [playerOneNames count] + [playerTwoNames count]);

  for (NSUInteger index = 0; index < [playerOneNames count]; index++) {
    XCTAssertTrue([playerNames containsObject:playerOneNames[index]]);
    XCTAssertTrue([playerNames containsObject:playerTwoNames[index]]);
  }
}

- (void)testRetrieveAllResults
{
  NSArray *playerNames = @[@"a", @"b", @"c", @"d", @"e"];
  NSArray *playerResults = @[@[@10, @0, @1],  // score  9 -> rank 1 ( a before c )
                             @[@40, @5, @6],  // score 34 -> rank 0
                             @[@17, @7, @8],  // score  9 -> rank 2 ( c after a )
                             @[@10,@11,@12],  // score -2 -> rank 4
                             @[@15,@14,@13]]; // score  2 -> rank 3
  NSArray *playerRanks = @[@1, @0, @2, @4, @3];

  for (NSUInteger index = 0; index < [playerNames count]; index++) {
    FakeGamePersistenceItem *item = [[FakeGamePersistenceItem alloc] init];
    item.playerName = playerNames[index];
    item.wins = [playerResults[index][0] unsignedIntegerValue];
    item.draws = [playerResults[index][1] unsignedIntegerValue];
    item.losses = [playerResults[index][2] unsignedIntegerValue];
    self.persistence.data[playerNames[index]] = item;
  }

  NSArray *highscores = [self.store allHighscores];

  XCTAssertNotNil(highscores);
  XCTAssertEqual([highscores count], [playerNames count]);

  for (NSUInteger index = 0; index < [playerNames count]; index++) {
    NSUInteger rank = [playerRanks[index] unsignedIntegerValue];

    id <GamePersistenceItem> persistenceItem = highscores[rank];
    XCTAssertEqualObjects([persistenceItem playerName], playerNames[index]);
    XCTAssertEqual([persistenceItem wins],    [playerResults[index][0] unsignedIntegerValue]);
    XCTAssertEqual([persistenceItem draws],   [playerResults[index][1] unsignedIntegerValue]);
    XCTAssertEqual([persistenceItem losses],  [playerResults[index][2] unsignedIntegerValue]);
  }
}

@end
