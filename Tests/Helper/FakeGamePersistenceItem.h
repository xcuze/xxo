//
//  FakeGamePersistenceItem.h
//  xxo
//
//  Created by Florian Krüger on 15/06/15.
//  Copyright (c) 2015 projectserver.org. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GamePersistenceItem.h"

@interface FakeGamePersistenceItem : NSObject <GamePersistenceItem>

@property (nonatomic, strong, readwrite) NSString *playerName;
@property (nonatomic, assign, readwrite) NSUInteger wins;
@property (nonatomic, assign, readwrite) NSUInteger draws;
@property (nonatomic, assign, readwrite) NSUInteger losses;

@end
